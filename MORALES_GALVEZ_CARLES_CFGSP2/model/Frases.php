<?php

class Frases{

    private $_idfrases;
    private $_frase;
    private $_autor;

    public function __construct($f, $a, $idfrases){
        $this->setfrase($f);
        $this->setAutor($a);
        $this->setIdfrases($idfrases);
    }

    public function getIdfrases()
    {
        return $this->_idfrases;
    }

    public function getFrase()
    {
        return $this->_frase;
    }

    public function getAutor()
    {
        return $this->_autor;
    }

    public function setIdfrases($_idfrases)
    {
        $this->_idfrases = $_idfrases;
    }

    public function setFrase($_frase)
    {
        $this->_frase = $_frase;
    }

    public function setAutor($_autor)
    {
        $this->_autor = $_autor;
    }

}
