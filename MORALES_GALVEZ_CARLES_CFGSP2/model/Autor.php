<?php

class Autor{

    private $_aid;
    private $_nom;
    private $_cognom;

    public function __construct($n, $c, $id = null){
        $this->setNom($n);
        $this->setCognom($c);
        $this->setAid($id);
    }

    public function getAid(){
        return $this->_aid;
    }

    public function getNom(){
        return $this->_nom;
    }

    public function getCognom(){
        return $this->_cognom;
    }

    public function setAid($id){
        $this->_aid = $id;
    }

    public function setNom($_nom){
        $this->_nom = $_nom;
    }

    public function setCognom($_cognom){
        $this->_cognom = $_cognom;
    }


}
