<?php

require_once(__DIR__.'/AutorDb.php');
require_once(__DIR__.'/../Frases.php');
require_once(__DIR__.'/../Autor.php');
require_once(__DIR__.'/../../inc/Constants.php');

class FrasesDb{

    private $conn;

    public function getFrase($fid){
        $this->openConnection();

        $sql = "SELECT * FROM frases WHERE fid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idfrases);
        $idfrases = $fid;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();
        $frases = new Frases($r['frase'], $r['idautor'], $r['fid']);

        return $frases;
    }

    public function getFrases(){
        $this->openConnection();

        $sql = "SELECT * FROM frases";
        $stm = $this->conn->prepare($sql);

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
          $frases = new Frases($r['frase'], $r['idautor'], $r['fid']);
          array_push($ret, $frases);

        return $ret;
      }
    }

    public function getFrasesById($idfrases){
        $this->openConnection();

        $sql = "SELECT * FROM frases WHERE fid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $idf);
        $idf = $idfrases;

        $stm->execute();
        $result = $stm->get_result();

        $row = $result->fetch_assoc();
        $frases = new Frases($row['frase'],$row['idautor'], $row['fid']);

        return $frases;
    }

    public function listFrasesByAutor($autorid){
        $dbautor = new AutorDb();
        $autorinstance = $dbautor->getAutor($autorid);

        $this->openConnection();

        $sql = "SELECT * FROM frases WHERE idautor = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $aid);
        $aid = $autorid;

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($row = $result->fetch_assoc()){
            $frases = new Frases($row['frase'],$row['idautor'],$row['fid']);
            array_push($ret, $frases);
        }
        return $ret;
    }

    public function insertFrases($f, $autorid){
        $this->openConnection();

        $sql = "INSERT INTO frases (frase, idautor) VALUES (?, ?)";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("si", $ff, $aid);
        $ff = $f;
        $aid = $autorid;

        $stm->execute();


        return null;

    }

    public function updateFrases($frase, $idfrases){
        $this->openConnection();

        $sql = "UPDATE frases SET frase = ? WHERE fid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("si", $ff, $fid);
        $ff= $frase;
        $fid = $idfrases;

        $stm->execute();

        return $this->getFrasesById($idfrases);
    }

    public function deleteFrases($idfrases){
        $frases = $this->getFrasesById($idfrases);
        $this->openConnection();

        $sql = "DELETE FROM frases WHERE fid  = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $fid);
        $fid = $idfrases;

        $stm->execute();

        return $frases;
    }


    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}
