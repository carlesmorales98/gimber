<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require(__DIR__.'/../vendor/autoload.php');
require_once(__DIR__.'/../lib/controller/UserController.php');

$app = new \Slim\App;

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");
    
    return $response;
});

$app->post('/hello/{name}', 
    function (Request $request, Response $response, array $args) {
        $name = $args['name'];
        $textResponse = array();
        $textResponse['hello'] = $name;
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($textResponse));
        
        return $newR;
    });

$app->post('/user',
    function(Request $request, Response $response, array $args) {
        $userjson = $request->getParsedBody();
        
        var_dump($userjson);
        die;
        
        $cnt = new UserController();
        $retcnt = $cnt->createUser($userjson['username']);
        
        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));
        
        return $newR;
        
    });

$app->run();