<?php

class User{

    private $_username;
    private $_followers;
    private $_following;
    private $_owntuits;

    public function __construct($un){
        $this->setUsername($un);
        $this->setFollowers([]);
        $this->setFollowing([]);
        $this->setOwntuits([]);
    }

    public function getUsername()
    {
        return $this->_username;
    }

    public function getFollowers()
    {
        return $this->_followers;
    }

    public function getFollowing()
    {
        return $this->_following;
    }

    public function getOwntuits()
    {
        return $this->_owntuits;
    }

    public function setUsername($_username)
    {
        $this->_username = $_username;
    }

    public function setFollowers($_followers)
    {
        $this->_followers = $_followers;
    }

    public function setFollowing($_following)
    {
        $this->_following = $_following;
    }

    public function setOwntuits($_owntuits)
    {
        $this->_owntuits = $_owntuits;
    }

    public function toArray(){
        $ret = array();
        $ret['username'] = $this->getUsername();
        $ret['followers'] = $this->getFollowers();
        $ret['following'] = $this->getFollowing();
        $ret['owntuits'] = $this->getOwntuits();

        return $ret;
    }

}
