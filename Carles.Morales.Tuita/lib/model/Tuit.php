<?php

class Tuit{

    private $_tuid;
    private $_msg;
    private $_usr;
    private $_fav;

    public function __construct($m,$u,$f,$tid = null){
        $this->setMsg($m);
        $this->setUsr($u);
        $this->setFav($f);
        $this->setTuid($tid);
    }

    public function getTuid()
    {
        return $this->_tuid;
    }

    public function setTuid($_tuid)
    {
        $this->_tuid = $_tuid;
    }

    public function getMsg()
    {
        return $this->_msg;
    }

    public function getUsr()
    {
        return $this->_usr;
    }

    public function getFav()
    {
        return $this->_fav;
    }

    public function setMsg($_msg)
    {
        $this->_msg = $_msg;
    }

    public function setUsr($_usr)
    {
        $this->_usr = $_usr;
    }

    public function setFav($_fav)
    {
        $this->_fav = $_fav;
    }

    public function toArray(){
        $ret = array();
        $ret['msg'] = $this->getMsg();
        $ret['usr'] = $this->getUsr();
        $ret['fav'] = $this->getFav();
        if(isset($this->_tuid)
                && $this->_tuid != null
                && $this->_tuid != "" ){
            $ret['tuid'] = $this->getTuid();
        }

        return $ret;
    }


}
