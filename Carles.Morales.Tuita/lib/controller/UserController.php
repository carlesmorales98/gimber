<?php

require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/db/UserDb.php');

class UserController{

    public function createUser($un){
        $user = new User($un);
        $db = new UserDb();

        $ret = $db->insertUser($user);

        return $ret->toArray();
    }

    public function removeUser($un){
        $db = new UserDb();

        $ret = $db->removeUser($un);

        return $ret->toArray();
    }

    public function addTuit2User($tuit){
        $db = new UserDb();
        $ret = $db->addTuit2User($tuit);

        return $ret;
    }

    public function fav($uname, $tuid){
        $db = new UserDb();
        $ret = $db->favTuit($uname, $tuid);

        return $ret;
    }

}
