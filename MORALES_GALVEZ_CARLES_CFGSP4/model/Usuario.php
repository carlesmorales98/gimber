<?php

class Usuario{

    private $_id;
    private $_nombre;
    private $_rol;

    public function __construct($n=null, $r=null, $i=null){
        $this->setId($i);
        $this->setNombre($n);
        $this->setRol($r);
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getNombre()
    {
        return $this->_nombre;
    }

    public function getRol()
    {
        return $this->_rol;
    }

    public function setId($_id)
    {
        $this->_id = $_id;
    }

    public function setNombre($_nombre)
    {
        $this->_nombre = $_nombre;
    }

    public function setRol($_rol)
    {
        $this->_rol = $_rol;
    }

    public function login($user){
        $_SESSION['usuario'] = $user;
    }

    public function logout(){
        unset($_SESSION['usuario']);
    }

    public function isLoggedIn(){
        if(isset($_SESSION['usuario'])
                && $_SESSION['usuario']!= null
                && $_SESSION['usuario']!= ""){
            return true;
        }
        return false;
    }

    public function rolin($rol){
      $_SESSION['rol'] = $rol;
    }



}
