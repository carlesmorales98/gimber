<?php

require_once(__DIR__.'/../Usuario.php');
require_once(__DIR__.'/../../inc/Constants.php');

class UsuarioDb{

    private $conn;

    public function login($usuario, $pwd){
        $this->openConnection();


        $sql = "SELECT * FROM usuario WHERE username =  ? AND password = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("ss", $uus, $upwd);
        $uus= $usuario;
        $upwd = $pwd;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();

        if($r != false){
            return true;
        }
        return false;

    }

    public function rolin($usuario, $pwd){
        $this->openConnection();

        $sql = "SELECT rol FROM usuario WHERE username =  ? AND password = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("ss", $uus, $upwd);
        $uus= $usuario;
        $upwd = $pwd;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();

        $rol = $r['rol'];

        return $rol;

    }

    


    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}
