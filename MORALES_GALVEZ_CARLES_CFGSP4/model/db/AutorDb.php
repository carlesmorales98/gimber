<?php

require_once(__DIR__.'/../Autor.php');
require_once(__DIR__.'/../../inc/Constants.php');


class AutorDb{

    private $conn;
    private $busqueda=array();

    public function getAutor($id){
        $this->openConnection();

        $sql = "SELECT * FROM autor WHERE aid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $autorid);
        $autorid = $id;

        $stm->execute();
        $result = $stm->get_result();

        $r = $result->fetch_assoc();
        $autor = new Autor($r['nom'], $r['cognom'], $r['aid']);

        return $autor;
    }

    public function getAutors(){
        $this->openConnection();

        $sql = "SELECT * FROM autor";
        $stm = $this->conn->prepare($sql);

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
            $autor = new Autor($r['nom'], $r['cognom'], $r['aid']);
            array_push($ret, $autor);
        }
        return $ret;
    }

    public function insertAutor($n, $c){
        $this->openConnection();

        $sql = "INSERT INTO autor (nom, cognom) VALUES (?, ?)";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("ss", $an, $ac);
        $an = $n;
        $ac = $c;

        $stm->execute();

        return new Autor($n, $c, null);
    }

    public function updateAutor($n, $c, $i){
        $this->openConnection();

        $sql = "UPDATE autor SET nom = ?, cognom = ? WHERE aid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("ssi", $an, $ac, $ai);
        $an = $n;
        $ac = $c;
        $ai = $i;

        $stm->execute();

        return $this->getAutor($i);
    }

    public function deleteAutor($id){
        $autor = $this->getAutor($id);
        $this->openConnection();

        $sql = "DELETE FROM autor WHERE aid = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("i", $aid);
        $aid = $id;

        $stm->execute();

        return $autor;
    }

    public function getAutorByName($n){
        $this->openConnection();

        $sql = "SELECT * FROM autor WHERE nom = ?";
        $stm = $this->conn->prepare($sql);

        $stm->bind_param("s", $nom);
        $nom = $n;

        $stm->execute();
        $result = $stm->get_result();

        $ret = array();
        while($r = $result->fetch_assoc()){
            $autor = new Autor($r['nom'], $r['cognom'], $r['aid']);
            array_push($ret, $autor);
        }
        return $ret;
    }


    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }

}
