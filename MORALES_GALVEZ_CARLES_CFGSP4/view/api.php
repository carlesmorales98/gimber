<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require(__DIR__.'/../vendor/autoload.php');
require_once(__DIR__.'/../controller/AutorController.php');
require_once(__DIR__.'/../controller/FrasesController.php');

$app = new \Slim\App;

/*$app->get('/autor',
    function(Request $request, Response $response, array $args) {
        $cnt = new AutorController();
        $retcnt = $cnt->getAutorN($n);

        $newarray = array();

        foreach ($retcnt as $autor){
            array_push($newarray, $autor->toArray());
        }

        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($newarray));

        return $newR;

    });*/

$app->post('/autor',
    function(Request $request, Response $response, array $args) {
        $autorjson = $request->getParsedBody();

        $cnt = new AutorController();
        $retcnt = $cnt->getAutorN($autorjson['nom']);

        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));

        return $newR;

    });

$app->get('/autor/{nom}',
    function(Request $request, Response $response, array $args) {
        $n = $args['nom'];
        $cnt = new AutorController();

        $aq = $cnt->getAutorN($n);

        $retcnt = array();

        foreach ($aq as $q) {
          array_push($retcnt,$aq);
        }

        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));

        return $newR;

    });

/*$app->delete('/autor/{nom}',
    function(Request $request, Response $response, array $args) {
        $id = $args['aid'];

        $cnt = new AutorController();
        $retcnt = $cnt->deleteAutor($id);

        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($retcnt));

        return $newR;

    });*/

$app->post('/autor/{nom}/frase',
    function(Request $request, Response $response, array $args) {
        $nom = $args['nom'];
        $msgjson = $request->getParsedBody();

        $tcnt = new FrasesController();
        $frase = $tcnt->getFrasesByAutor($msgjson['idfrase'], $nom);

        $ucnt = new AutorController();
        $user = $ucnt->getAutorN($n);

        $newR = $response->withHeader('Content-type', 'application/json');
        $newR->getBody()->write(json_encode($fid));

        return $newR;

    });


//HTML Views

$app->get('/html/loginform',
    function(Request $request, Response $response, array $args) {
        //$form = '<form>';
        $form = '<label for="un">Buscar Autor</label>';
        $form .= '<input type="text" id="hp-autor"/>';
        $form .= '<button id="hp-autorB">busca</button>';
        //$form .= '</form>';
        $newR = $response->withHeader('Content-type', 'text/html');
        $newR->getBody()->write(($form));

        return $newR;
    });

$app->run();
