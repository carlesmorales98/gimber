<?php

require_once(__DIR__.'/../model/db/AutorDb.php');
require_once(__DIR__.'/../model/Autor.php');

class AutorController{

  public function getAutor($id){
        $db = new AutorDb();
        return $db->getAutor($id);
    }

    public function getAutorN($n){
      $db = new AutorDb();
      $cnt = new FrasesDb();

      $autors = $db->getAutorByName($n);
      $autorAso = array();
      $newfrases = array();
      $fraseAso = array();
      $ret = array();


      foreach ($autors as $a) {
        $frases = $cnt->listFrasesByAutor($a->getAid());
        array_push($autorAso, $a->toArray());

        foreach ($frases as $f) {
          array_push($fraseAso, $f);
        }
      }

      foreach ($fraseAso as $frase) {
        array_push($newfrases, $frase->toArray());
      }

        $ret["autor"] = $autorAso;
        $ret["frases"] = $newfrases;

        return $ret;
      }


    public function getAutors($random = true){
        $db = new AutorDb();
        $llistaoriginal = $db->getAutors();
        return $llistaoriginal;
    }

    public function createAutor($n, $c){
        $db = new AutorDb();
        return $db->insertAutor($n, $c);
    }


    public function deleteAutor($id){
        $db = new AutorDb();
        return $db->deleteAutor($id);
    }


    private function randomizeList($llistaoriginal){
        $llista = array();
        $reps = count($llistaoriginal);
        for($i = 0 ; $i < $reps ; $i++){
            $indexr = rand(0, count($llistaoriginal)-1);
            array_push($llista, $llistaoriginal[$indexr]);
            $newlist = array();
            for($ii = 0 ; $ii < count($llistaoriginal) ; $ii++){
                if($ii != $indexr){
                    array_push($newlist, $llistaoriginal[$ii]);
                }
                $llistaoriginal = $newlist;
            }
        }
    }

}
