<?php

require_once(__DIR__.'/../model/Usuario.php');
require_once(__DIR__.'/../model/db/UsuarioDb.php');

class UserController{

    public function login($usuario, $pwd){
        $salted = md5(Constants::$SALT_BIT);
        $salted = $salted.$pwd;
        $def = md5($salted);

        $db = new UsuarioDb();
        if($db->login($usuario, $def)){
            $model = new Usuario();
            $model->login($usuario);
            return true;
        }
        return false;
    }

    public function logout(){

    }

    public function rolin($usuario, $pwd){
        $salted = md5(Constants::$SALT_BIT);
        $salted = $salted.$pwd;
        $def = md5($salted);

        $db = new UsuarioDb();
        $rol = $db->rolin($usuario, $def);
            $model = new Usuario();
            $model->rolin($rol);

    }

}
