<?php

require_once(__DIR__.'/../model/Frase.php');

class IndexController{

  private $nom = ['Pitagoras', 'Seneca', 'Kant', 'Platon'];

  private $categoria = ['Pitagoras'=>['Educacion -> '],
                        'Seneca'=>['Amor -> '],
                        'Kant'=>['Educacion -> '],
                        'Platon'=>['Libertad -> ']];

  private $frase = ['Seneca' => ['Amor -> ' => ['La amistad siempre es provechosa, el amor a veces hiere.']],
                    'Pitagoras' => ['Educacion -> ' => ['Educad a los niños y no será necesario castigar a los hombres.']],
                    'Kant' => ['Educacion -> ' => ['El sabio puede cambiar de opnión. El necio, nunca.']],
                    'Platon' =>['Libertad -> ' => ['La libertad consiste en ser dueños de la propia vida.']]];

      public function Frases($ord=4){
                $ret = array();
                  for($i = 0 ; $i < $ord ; $i++){
                    $n = $this->nom[$i];
                    for($j=0; $j<count($this->categoria[$n]); $j++){
                      $c = $this->categoria[$n][$j];
                      for($k=0; $k<count($this->frase[$n][$c]); $k++){
                        $f = $this->frase[$n][$c][$k];
                      }
                    }

                        $reta = new Frase($n, $c, $f);
                        array_push($ret, $reta);
                    }
                    return $ret;
      }

}
