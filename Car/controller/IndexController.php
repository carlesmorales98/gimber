<?php
require_once(__DIR__.'/../model/Car.php');

class IndexController{

    private $brands = ['BMW', "Mercedes", "VW", "Renault"];
    private $models = [ 'BMW' => ["m3", "m30", "z1", "Serie 1"],
                        'Mercedes' => ["AMG", "SLK", "SLS", "AMG GT"],
                        'VW' => ["Golf", "Polo", "Sirocco"],
                        'Renault' => ["Clio", "Megane", "Espace", "Scenic", "Twingo"]];
    private $gas = ["unleaded", "diesel"];
    private $colors = ["red", "blue", "white", "black"];

    public function randomListAction($ord = 10){
        $ret = array();
        for($i = 0 ; $i < $ord ; $i++){
            $indexb = rand(0, count($this->brands)-1);
            $b = $this->brands[$indexb];
            
            $indexm = rand(0, count($this->models[$b])-1);
            $m = $this->models[$b][$indexm];

            $indexg = rand(0, count($this->gas)-1);
            $g = $this->gas[$indexg];

            $indexc = rand(0, count($this->colors)-1);
            $c = $this->colors[$indexc];

            $reta = new Car($b, $m, $g, $c);
            array_push($ret, $reta);
        }
        return $ret;
    }

}
