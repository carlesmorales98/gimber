<?php

require_once(__DIR__.'/../../controller/GameCController.php');

$cn = $_POST['cname'];
$cs = $_POST['cspec'];
$ca = $_POST['cap'];
$cd = $_POST['cdp'];

$cnt = new GameCController();
$cnt->createCharacter($cn, $cs, $ca, $cd);

?><html>
	<head>
		<title>Create new character</title>
	</head>
	<body>
		<div id="wrapper">
			<h1><?=$cn?> Added</h1>
			<ul>
				<li>Name:<?=$cn?></li>
				<li>Specie:<?=$cs?></li>
				<li>Attack Points:<?=$ca?></li>
				<li>Defense Points:<?=$cd?></li>
			</ul>
			<a href="/">Go back to the Home page</a>
		</div>
	</body>
</html>
