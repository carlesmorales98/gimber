<?php

require_once(__DIR__.'/../Team.php');

class TeamDb{

  public function findAllTeams(){
    $collection = (new MongoDB\Client)->football->team;
    $cursor = $collection->find([]);

    $ret = array();
    foreach ($cursor as $mongoteam){
        $stname = $mongoteam['stadium']['name'];
        $stloc = array();
        $stloc[0] = $mongoteam['stadium']['loc']['long'];
        $stloc[1] = $mongoteam['stadium']['loc']['lat'];

        $stadium = new Stadium($stname, $stloc);

        $tename = $mongoteam['name'];
        $tefound = $mongoteam['founded'];

        $team = new Team($tename, $tefound, $stadium);
        array_push($ret, $team);
    }
    return $ret;
}

    /**
     * Param should be a Team class instance.
     *
     * @param Team $team
     */
    public function insertTeam($team){
        $collection = (new MongoDB\Client)->football->team;

        $insertOneResult = $collection->insertOne($team->toArray());

        return $insertOneResult->getInsertedId();
    }

}
