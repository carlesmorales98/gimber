<?php
require_once(__DIR__.'/../controller/controller.php');
$c = new controller();

$list = $c->listCharacters();

 ?><html>
   <head>
     <title>Formulari</title>
   </head>
   <body>
     <div id="wrapper">
       <div id="homeform">
         <dl>

           <!-- Nombre -->
           <dt><label for="nombre">Nombre</label></dt>
           <dd><input type="text" id="nombre" name="nombre"/></dd>

           <!--Espacie-->
           <dt><label for="especie">Especie</dt>
             <dd>
               <select id="especie" name="especie">
                 <option value="Pato">Pato</option>
                 <option value="Gato">Gato</option>
                 <option value="Rana">Rana</option>
                 <option value="Persona">Persona</option>
               </select>
             </dd>

            <!--Ataque -->
            <dt><label for="pda">Puntos de ataque</dt>
            <dd><input type="numeric" id="pda" name="pda"/></dd>

            <!--Defense-->
            <dt><label for="pdd">Puntos de defensa</label></dt>
            <dd><input type="numeric" id="pdd" name="pdd"/></dd>
            <dd><input type="submit" name="csubmit" value="Add"/></dd>
					</dl>
				</form>
			</div>
			<hr/>
    		<h2>Lista de Personajes: </h2>
    		<ul>
    		<?php foreach($list as $g){ ?>
    		<li><?=$g->getName()?>:<?=$g->getSpecie()?></li>
    		<?php } ?>
    		</ul>
		</div>
	</body>
</html>
