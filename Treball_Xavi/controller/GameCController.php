<?php
require_once(__DIR__.'/../model/db/GameCDb.php');

class GameCController{

    /**
     * Create a character given all the attributes of the class
     *
     * @param String $name
     * @param String $specie
     * @param Integer $attack
     * @param Integer $defense
     * @return boolean
     */
    public function createCharacter($name, $specie,
            $attack = null, $defense = null){

        //Create GameC instance
        $gc = new GameC($name, $specie);
        $gc->setDefenseP($defense);
        $gc->setAttackP($attack);

        //Instantiate GameCDb
        $db = new GameCDb();

        //Create DB registry and return
        return $db->createGameC($gc);
    }

    /**
     * Gets all the GameC registries from the DB
     *
     * @return array of GameC instances
     */
    public function listCharacters(){
        $db = new GameCDb();
        return $db->listGameC();
    }

    /**
     * Gets one GameC instance given its DB id
     *
     * @param Integer $id
     * @return GameC instance
     */
    public function getCharacter($id){
        $db = new GameCDb();
        return $db->getGameC($id);
    }

    public function modifyCharacter(){

    }


    /**
     * Remove GameC instance from DB given its id
     *
     * @param Integer $id
     * @return GameC instance
     */
    public function removeCharacter($id){
        $db = new GameCDb();
        return $db->removeGameC($id);
    }

}
