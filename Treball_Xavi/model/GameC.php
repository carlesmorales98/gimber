<?php

class GameC{

    private $_id;
    private $_name;
    private $_defenseP;
    private $_attackP;
    private $_specie;

    public function __construct($n=null, $s=null){
        $this->setName($n);
        $this->setSpecie($s);
    }

    public function getId(){
        return $this->_id;
    }

    public function getName(){
        return $this->_name;
    }

    public function getDefenseP(){
        return $this->_defenseP;
    }

    public function getAttackP(){
        return $this->_attackP;
    }

    public function getSpecie(){
        return $this->_specie;
    }

    public function setId($id){
        $this->_id = $id;
    }

    public function setName($_name){
        $this->_name = $_name;
    }

    public function setDefenseP($_defenseP){
        $this->_defenseP = $_defenseP;
    }

    public function setAttackP($_attackP){
        $this->_attackP = $_attackP;
    }

    public function setSpecie($_specie){
        $this->_specie = $_specie;
    }

}
