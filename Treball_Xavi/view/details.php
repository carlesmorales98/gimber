<?php

require_once(__DIR__.'/../controller/GameCController.php');
$cnt = new GameCController();
$idc = $_GET['c'];

$gc = $cnt->getCharacter($idc);

?><html>
	<head>
	</head>
	<body>
		<div id="wrapper">
    		<h2><?=$gc->getName()?> Details</h2>
    		<ul>
    			<li>Name:<?=$gc->getName()?></li>
				<li>Specie:<?=$gc->getSpecie()?></li>
				<li>Attack Points:<?=$gc->getAttackP()?></li>
				<li>Defense Points:<?=$gc->getDefenseP()?></li>
    		</ul>
    		<a href="/">Go back to the Home page</a>
		</div>
	</body>
</html>
